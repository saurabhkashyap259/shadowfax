package com.mobile.shadowfax.adapter.viewholders;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.mobile.shadowfax.R;
import com.mobile.shadowfax.models.City;

public class CityViewHolder extends ChildViewHolder {

    public TextView cityNameTV;
    public TextView cityRankTV;

    public CityViewHolder(View itemView) {
        super(itemView);

        cityNameTV = itemView.findViewById(R.id.item_city_name_tv);
        cityRankTV = itemView.findViewById(R.id.item_city_rank_tv);
    }

    public void setData(City city, int position) {
        if(city != null) {
            cityNameTV.setText(city.getName());
            String rank = String.valueOf(position+1);
            cityRankTV.setText(String.valueOf(rank));
        }
    }
}
