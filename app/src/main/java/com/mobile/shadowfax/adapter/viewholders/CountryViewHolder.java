package com.mobile.shadowfax.adapter.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CountryExpandableAdapter;
import com.mobile.shadowfax.models.Country;

public class CountryViewHolder extends ParentViewHolder {

    public TextView countryNameTV;
    public ImageView editIV;

    public CountryViewHolder(View itemView) {
        super(itemView);

        countryNameTV = itemView.findViewById(R.id.item_country_name_tv);
        editIV = itemView.findViewById(R.id.item_country_edit_iv);
    }

    public void setData(final Country country
            , final CountryExpandableAdapter.InterfaceCountryExpandableAdapter interfaceCountryExpandableAdapter, final int position) {
        if(country != null) {
            countryNameTV.setText(country.getName());

            editIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interfaceCountryExpandableAdapter.onEditClick(country, position);
                }
            });
        }
    }
}
