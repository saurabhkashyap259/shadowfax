package com.mobile.shadowfax.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.viewholders.CityViewHolder;
import com.mobile.shadowfax.adapter.viewholders.CountryViewHolder;
import com.mobile.shadowfax.models.City;
import com.mobile.shadowfax.models.Country;

import java.util.List;

public class CountryExpandableAdapter extends ExpandableRecyclerAdapter<Country, City, CountryViewHolder, CityViewHolder> {

    private InterfaceCountryExpandableAdapter interfaceCountryExpandableAdapter;

    public CountryExpandableAdapter(@NonNull List<Country> parentList) {
        super(parentList);
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View view = LayoutInflater.from(parentViewGroup.getContext())
                .inflate(R.layout.item_country, parentViewGroup, false);
        return new CountryViewHolder(view);
    }

    @Override
    public CityViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(@NonNull CountryViewHolder parentViewHolder, int parentPosition, @NonNull Country parent) {
        parentViewHolder.setData(parent, interfaceCountryExpandableAdapter, parentPosition);
    }

    @Override
    public void onBindChildViewHolder(@NonNull CityViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull City child) {
        childViewHolder.setData(child, childPosition);
    }

    public interface InterfaceCountryExpandableAdapter {
        void onEditClick(Country country, int position);
    }

    public void setInterfaceCountryExpandableAdapter(InterfaceCountryExpandableAdapter listener) {
        this.interfaceCountryExpandableAdapter = listener;
    }
}
