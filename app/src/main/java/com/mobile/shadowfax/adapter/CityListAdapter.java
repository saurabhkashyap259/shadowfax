package com.mobile.shadowfax.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.viewholders.CityListViewHolder;
import com.mobile.shadowfax.adapter.viewholders.CityViewHolder;
import com.mobile.shadowfax.models.City;

import java.util.List;

public class CityListAdapter extends RecyclerView.Adapter<CityListViewHolder> {

    private Context context;
    private List<City> cityList;
    private InterfaceCityListAdapter interfaceCityListAdapter;

    public CityListAdapter(Context context, List<City> cityList) {
        this.context = context;
        this.cityList = cityList;
    }

    @NonNull
    @Override
    public CityListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_city_list, viewGroup, false);
        return new CityListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CityListViewHolder cityListViewHolder, int i) {
        cityListViewHolder.setData(cityList.get(i), interfaceCityListAdapter);
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    public interface InterfaceCityListAdapter {
        void onItemClick(CityListViewHolder holder, City city);
    }

    public void setInterfaceCityListAdapter(InterfaceCityListAdapter listener) {
        this.interfaceCityListAdapter = listener;
    }
}
