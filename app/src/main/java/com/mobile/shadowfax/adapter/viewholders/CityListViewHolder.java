package com.mobile.shadowfax.adapter.viewholders;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CityListAdapter;
import com.mobile.shadowfax.models.City;

public class CityListViewHolder extends RecyclerView.ViewHolder {

    public TextView cityNameTV;
    public ConstraintLayout rootLayout;
    public TextView cityRankTV;

    public CityListViewHolder(@NonNull View itemView) {
        super(itemView);

        cityNameTV = itemView.findViewById(R.id.item_city_list_name_tv);
        rootLayout = itemView.findViewById(R.id.item_city_list_root_layout);
        cityRankTV = itemView.findViewById(R.id.item_city_list_rank_tv);
    }

    public void setData(final City city, final CityListAdapter.InterfaceCityListAdapter interfaceCityListAdapter) {
        if(city != null) {
            cityNameTV.setText(city.getName());
            if(city.isRanked()) {
                cityRankTV.setVisibility(View.VISIBLE);
                cityRankTV.setText(String.valueOf(city.getRank()));
            }else {
                cityRankTV.setVisibility(View.GONE);
            }
            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!city.isRanked()) {
                        interfaceCityListAdapter.onItemClick(CityListViewHolder.this, city);
                    }
                }
            });
        }
    }
}
