package com.mobile.shadowfax.adapter.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CountryListAdapter;
import com.mobile.shadowfax.models.Country;

import java.util.List;

public class CountryListViewHolder extends RecyclerView.ViewHolder {

    public TextView name;
    public CheckBox checkBox;

    public CountryListViewHolder(@NonNull View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.item_country_list_name_tv);
        checkBox = itemView.findViewById(R.id.item_country_list_cb);
    }

    public void setData(final Country country, List<String> selectedCountryList, final CountryListAdapter.InterfaceCountryListAdapter interfaceCountryListAdapter) {
        if(country != null) {
            name.setText(country.getName());

            checkBox.setChecked(false);
            if(selectedCountryList.contains(String.valueOf(country.getId()))) {
                checkBox.setChecked(true);
            }

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    interfaceCountryListAdapter.onCheckChanged(country, isChecked);
                }
            });
        }
    }
}
