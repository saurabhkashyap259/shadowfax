package com.mobile.shadowfax.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.viewholders.CountryListViewHolder;
import com.mobile.shadowfax.models.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListViewHolder> {

    private Context context;
    private List<Country> countryList;
    private List<String> selectedCountryIdList = new ArrayList<>();
    private InterfaceCountryListAdapter interfaceCountryListAdapter;

    public CountryListAdapter(Context context, List<Country> countryList) {
        this.context = context;
        this.countryList = countryList;
    }

    public void setSelectedCountryIdList(List<String> selectedCountryIdList) {
        this.selectedCountryIdList.addAll(selectedCountryIdList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CountryListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_country_list, viewGroup, false);
        return new CountryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryListViewHolder countryListViewHolder, int i) {
        countryListViewHolder.setData(countryList.get(i), selectedCountryIdList, interfaceCountryListAdapter);
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public interface InterfaceCountryListAdapter {
        void onCheckChanged(Country country, boolean isChecked);
    }

    public void setInterfaceCountryListAdapter(InterfaceCountryListAdapter listener) {
        this.interfaceCountryListAdapter = listener;
    }
}