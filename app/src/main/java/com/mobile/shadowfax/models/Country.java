package com.mobile.shadowfax.models;

import android.os.Parcel;
import android.os.Parcelable;


import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

public class Country implements Parent<City>, Parcelable {

    private int id;
    private String name;
    private boolean isSelected;
    private List<City> cityList;

    public Country(int id, String name, boolean isSelected, List<City> cityList) {
        this.id = id;
        this.name = name;
        this.isSelected = isSelected;
        this.cityList = cityList;
    }

    protected Country(Parcel in) {
        id = in.readInt();
        name = in.readString();
        isSelected = in.readByte() != 0;
        cityList = new ArrayList<>();
        Class<?> type = (Class<?>) in.readSerializable();
        in.readList(cityList, type.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        final Class<?> objectsType = cityList.get(0).getClass();
        dest.writeSerializable(objectsType);
        dest.writeList(cityList);
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    @Override
    public List<City> getChildList() {
        return cityList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
