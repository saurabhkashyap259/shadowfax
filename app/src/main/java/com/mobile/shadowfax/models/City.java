package com.mobile.shadowfax.models;

import android.os.Parcel;
import android.os.Parcelable;

public class City implements Parcelable {

    private int id;
    private String name;
    private int rank;
    private boolean isRanked;

    public City(int id, String name, int rank) {
        this.id = id;
        this.name = name;
        this.rank = rank;
    }

    protected City(Parcel in) {
        id = in.readInt();
        name = in.readString();
        rank = in.readInt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public boolean isRanked() {
        return isRanked;
    }

    public void setRanked(boolean ranked) {
        isRanked = ranked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(rank);
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };
}
