package com.mobile.shadowfax.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CityListAdapter;
import com.mobile.shadowfax.adapter.viewholders.CityListViewHolder;
import com.mobile.shadowfax.adapter.viewholders.CityViewHolder;
import com.mobile.shadowfax.models.City;
import com.mobile.shadowfax.models.Country;

import java.util.ArrayList;
import java.util.List;

public class CityListActivity extends AppCompatActivity {

    private CityListAdapter adapter;
    private List<City> cityList = new ArrayList<>();
    private int rankCounter = 0;
    private Country country;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            country = bundle.getParcelable("country");
            if (country != null) {
                cityList.addAll(country.getCityList());
            }else {
                finish();
            }
        }

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Reorder Cities");
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CityListAdapter(this, cityList);
        recyclerView.setAdapter(adapter);

        adapter.setInterfaceCityListAdapter(new CityListAdapter.InterfaceCityListAdapter() {
            @Override
            public void onItemClick(CityListViewHolder holder, City city) {
                if(rankCounter < cityList.size()) {
                    cityList.remove(city);
                    city.setRank(rankCounter + 1);
                    city.setRanked(true);
                    cityList.add(rankCounter, city);
                    holder.cityRankTV.setVisibility(View.VISIBLE);
                    rankCounter++;
                    adapter.notifyDataSetChanged();
                }
            }
        });

        Button button = findViewById(R.id.button);
        button.setText("Save");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });

        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Reorder cities")
                .setMessage("Click on city names in increasing order of their rank. Click city with rank 1 first and rank 5 last.")
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void finishActivity() {
        country.setCityList(cityList);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable("country", country);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }
}
