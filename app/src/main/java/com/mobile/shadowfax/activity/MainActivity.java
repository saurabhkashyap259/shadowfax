package com.mobile.shadowfax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.mobile.shadowfax.factory.CountryDataFactory;
import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CountryExpandableAdapter;
import com.mobile.shadowfax.models.Country;
import com.mobile.shadowfax.utils.Constants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private ArrayList<Country> selectedCountry = new ArrayList<>();
    private ArrayList<String> selectedCountryIds = new ArrayList<>();
    private CountryExpandableAdapter adapter;
    private int editingCountryPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CountryExpandableAdapter(selectedCountry);

        recyclerView.setAdapter(adapter);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCountryListActivity();
            }
        });

        adapter.setInterfaceCountryExpandableAdapter(new CountryExpandableAdapter.InterfaceCountryExpandableAdapter() {
            @Override
            public void onEditClick(Country country, int postion) {
                startCityListActivity(country, postion);
            }
        });
    }

    private void startCountryListActivity() {
        Intent intent = new Intent(MainActivity.this, CountryListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.ARG_SELECT_COUNTRY_ID, selectedCountryIds);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_CODE_COUNTY_LIST);
    }

    private void startCityListActivity(Country country, int position) {
        editingCountryPosition = position;
        Intent intent = new Intent(MainActivity.this, CityListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("country", country);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_CODE_CITY_LIST);
    }

    private void setSelectedCountry(Bundle bundle) {
        ArrayList<String> countryIdArrayList = bundle.getStringArrayList(Constants.ARG_SELECT_COUNTRY_ID);
        if(countryIdArrayList != null) {
            selectedCountry.clear();
            selectedCountryIds.clear();
            selectedCountryIds.addAll(countryIdArrayList);
            for(String id : countryIdArrayList) {
                selectedCountry.add(CountryDataFactory.getCountryById(Integer.parseInt(id)));
            }
            adapter.notifyParentDataSetChanged(false);
        }
    }

    private void setCityRank(Bundle bundle) {
        Country country = bundle.getParcelable("country");
        if(country != null) {
            if(editingCountryPosition != -1) {
                Country editingCountry = selectedCountry.get(editingCountryPosition);
                editingCountry.setCityList(country.getCityList());
                CountryDataFactory.setCitiesByCountry(country);
                adapter.notifyParentChanged(editingCountryPosition);
                editingCountryPosition = -1;
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_COUNTY_LIST:
                    if(data != null) {
                        Bundle bundle = data.getExtras();
                        if(bundle != null) {
                            setSelectedCountry(bundle);
                        }
                    }
                    break;

                case Constants.REQUEST_CODE_CITY_LIST:
                    if(data != null) {
                        Bundle bundle = data.getExtras();
                        if(bundle != null) {
                            setCityRank(bundle);
                        }
                    }
                    break;
            }
        }
    }
}
