package com.mobile.shadowfax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mobile.shadowfax.factory.CountryDataFactory;
import com.mobile.shadowfax.R;
import com.mobile.shadowfax.adapter.CountryListAdapter;
import com.mobile.shadowfax.models.Country;
import com.mobile.shadowfax.utils.Constants;

import java.util.ArrayList;

public class CountryListActivity extends AppCompatActivity {

    private final String TAG = CountryListActivity.class.getSimpleName();
    private ArrayList<String> selectedCountryIds = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            ArrayList<String> countryIdList = bundle.getStringArrayList(Constants.ARG_SELECT_COUNTRY_ID);
            if(countryIdList != null)
                Log.e(TAG, "Country id list not null: " + countryIdList);
                selectedCountryIds.addAll(countryIdList);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Select Countries");
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        CountryListAdapter adapter = new CountryListAdapter(this, CountryDataFactory.getCountries());
        recyclerView.setAdapter(adapter);

        adapter.setSelectedCountryIdList(selectedCountryIds);

        Button button = findViewById(R.id.button);
        button.setText("Save");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });

        adapter.setInterfaceCountryListAdapter(new CountryListAdapter.InterfaceCountryListAdapter() {
            @Override
            public void onCheckChanged(Country country, boolean isChecked) {
                if(isChecked) {
                    country.setSelected(true);
                    if(!selectedCountryIds.contains(String.valueOf(country.getId()))) {
                        selectedCountryIds.add(String.valueOf(country.getId()));
                    }
                }else {
                    country.setSelected(false);
                    selectedCountryIds.remove(String.valueOf(country.getId()));
                }
            }
        });
    }

    private void finishActivity() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.ARG_SELECT_COUNTRY_ID, selectedCountryIds);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }
}
