package com.mobile.shadowfax.factory;

import com.mobile.shadowfax.models.City;
import com.mobile.shadowfax.models.Country;

import java.util.Arrays;
import java.util.List;

public class CountryDataFactory {

    private static List<City> indianCityList;
    private static List<City> usaCityList;
    private static List<City> germanyCityList;
    private static List<City> canadaCityList;
    private static List<City> australiaCityList;
    private static List<City> ukCityList;
    private static List<City> southAfricaCityList;

    public static List<Country> getCountries() {
        return Arrays.asList(
                getIndia(),
                getUSA(),
                getGermany(),
                getCanada(),
                getAustralia(),
                getUk(),
                getSouthAfrica()
        );
    }

    public static Country getCountryById(int id) {
        switch (id) {
            case 1:
                return getIndia();
            case 2:
                return getUSA();
            case 3:
                return getGermany();
            case 4:
                return getCanada();
            case 5:
                return getAustralia();
            case 6:
                return getUk();
            case 7:
                return getSouthAfrica();
            default:
                return null;
        }
    }

    private static Country getIndia() {
        return new Country( 1,"India", false, getIndianCityList());
    }

    private static Country getUSA() {
        return new Country(2, "USA", false, getUsaCityList());
    }

    private static Country getGermany() {
        return new Country(3,"Germany", false, getGermanyCityList());
    }

    private static Country getCanada() {
        return new Country(4, "Canada", false, getCanadaCityList());
    }

    private static Country getAustralia() {
        return new Country(5, "Australia", false, getAustraliaCityList());
    }

    private static Country getUk() {
        return new Country(6, "United Kingdom", false, getUkCityList());
    }

    private static Country getSouthAfrica() {
        return new Country(7, "South Africa", false, getSouthAfricaCityList());
    }

    public static List<City> getIndianCityList() {
        return indianCityList != null ? indianCityList : getIndianCities();
    }

    public static void setIndianCityList(List<City> indianCityList) {
        CountryDataFactory.indianCityList = indianCityList;
    }

    public static List<City> getUsaCityList() {
        return usaCityList != null ? usaCityList : getUSACities();
    }

    public static void setUsaCityList(List<City> usaCityList) {
        CountryDataFactory.usaCityList = usaCityList;
    }

    public static List<City> getGermanyCityList() {
        return germanyCityList != null ? germanyCityList : getGermanyCities();
    }

    public static void setGermanyCityList(List<City> germanyCityList) {
        CountryDataFactory.germanyCityList = germanyCityList;
    }

    public static List<City> getCanadaCityList() {
        return canadaCityList != null ? canadaCityList : getCanadaCities();
    }

    public static void setCanadaCityList(List<City> canadaCityList) {
        CountryDataFactory.canadaCityList = canadaCityList;
    }

    public static List<City> getAustraliaCityList() {
        return australiaCityList != null ? australiaCityList : getAustraliaCities();
    }

    public static void setAustraliaCityList(List<City> australiaCityList) {
        CountryDataFactory.australiaCityList = australiaCityList;
    }

    public static List<City> getUkCityList() {
        return ukCityList != null ? ukCityList : getUkCities();
    }

    public static void setUkCityList(List<City> ukCityList) {
        CountryDataFactory.ukCityList = ukCityList;
    }

    public static List<City> getSouthAfricaCityList() {
        return southAfricaCityList != null ?southAfricaCityList : getSouthAfricaCities();
    }

    public static void setSouthAfricaCityList(List<City> southAfricaCityList) {
        CountryDataFactory.southAfricaCityList = southAfricaCityList;
    }

    private static List<City> getIndianCities() {
        return Arrays.asList(
                new City(1, "Delhi", 1),
                new City(2, "Mumbai", 2),
                new City(3, "Bangalore", 3),
                new City(4, "Kolkata", 4),
                new City(5, "Chennai", 5)
        );
    }

    private static List<City> getUSACities() {
        return Arrays.asList(
                new City(6, "New York", 1),
                new City(7, "New Jersy", 2),
                new City(8, "Boston", 3),
                new City(9, "San Jose", 4),
                new City(10, "San Mateo", 5)
        );
    }

    private static List<City> getGermanyCities() {
        return Arrays.asList(
                new City(11, "Berlin", 1),
                new City(12, "Munich", 2),
                new City(13, "Frankfurt", 3),
                new City(14, "Hamburg", 4),
                new City(15, "Dresden", 5)
        );
    }

    private static List<City> getCanadaCities() {
        return Arrays.asList(
                new City(16, "Toronto", 1),
                new City(17, "Montreal", 2),
                new City(18, "Vancouver", 3),
                new City(19, "Calgary", 4),
                new City(20, "Edmonton", 5)
        );
    }

    private static List<City> getAustraliaCities() {
        return Arrays.asList(
                new City(21, "Sydney", 1),
                new City(22, "Melbourne", 2),
                new City(23, "Perth", 3),
                new City(24, "Cairns", 4),
                new City(25, "Hobart", 5)
        );
    }

    private static List<City> getUkCities() {
        return Arrays.asList(
                new City(26, "London", 1),
                new City(27, "Edinburgh", 2),
                new City(28, "Manchester", 3),
                new City(29, "Birmingham", 4),
                new City(30, "Glasgow", 5)
        );
    }

    private static List<City> getSouthAfricaCities() {
        return Arrays.asList(
                new City(31, "Cape Town", 1),
                new City(32, "Johannesburg", 2),
                new City(33, "Durban", 3),
                new City(34, "Pretoria", 4),
                new City(35, "Port Elizabeth", 5)
        );
    }

    public static void setCitiesByCountry(Country country) {
        switch (country.getId()) {
            case 1:
                setIndianCityList(country.getCityList());
                break;

            case 2:
                setUsaCityList(country.getCityList());
                break;

            case 3:
                setGermanyCityList(country.getCityList());
                break;

            case 4:
                setCanadaCityList(country.getCityList());
                break;

            case 5:
                setAustraliaCityList(country.getCityList());
                break;

            case 6:
                setUkCityList(country.getCityList());
                break;

            case 7:
                setSouthAfricaCityList(country.getCityList());
                break;

        }
    }
}
