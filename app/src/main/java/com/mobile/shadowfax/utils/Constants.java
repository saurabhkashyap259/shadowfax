package com.mobile.shadowfax.utils;

public class Constants {

    public static final int REQUEST_CODE_COUNTY_LIST = 40001;
    public static final int REQUEST_CODE_CITY_LIST = 40002;

    public static final String ARG_SELECT_COUNTRY_ID = "selected_country_id";
}
